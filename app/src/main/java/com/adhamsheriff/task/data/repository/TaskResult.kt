package com.adhamsheriff.task.data.repository

sealed class TaskResult<out T> {
    data class Failure(val codes: ErrorCodes) : TaskResult<Nothing>()
    data class Success<out T>(val result: T) : TaskResult<T>()
}

enum class ErrorCodes {
    OTHERS,
    NETWORK_FAILURE,
    API_FAILURE,
    ACCESS_DENIED
}