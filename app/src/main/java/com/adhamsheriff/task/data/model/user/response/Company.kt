package com.adhamsheriff.task.data.model.user.response

data class Company(
    val bs: String,
    val catchPhrase: String,
    val name: String
)