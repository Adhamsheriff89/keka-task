package com.adhamsheriff.task.data.domain

import com.adhamsheriff.task.data.model.user.response.UserResponseItem
import com.adhamsheriff.task.data.repository.ErrorCodes
import com.adhamsheriff.task.data.repository.TaskResult
import com.adhamsheriff.task.data.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class UsersDataUseCase @Inject constructor(
    private val userRepository: UserRepository
) {

    sealed interface UserDetailResult<out T> {
        data class Success<out T>(
            val result: T
        ) : UserDetailResult<T>
        object OtherFailure : UserDetailResult<Nothing>
        object AccessDenied : UserDetailResult<Nothing>
    }


    suspend fun fetchUserListFromNetwork(
    ): UserDetailResult<List<UserResponseItem>>{
        return withContext(Dispatchers.IO) {
            val networkFetchResult = userRepository.fetchUserList()
            if (networkFetchResult is TaskResult.Success) {
                return@withContext UserDetailResult.Success(networkFetchResult.result)
            } else if (networkFetchResult is TaskResult.Failure && networkFetchResult.codes == ErrorCodes.API_FAILURE) {
                return@withContext UserDetailResult.OtherFailure
            }else if (networkFetchResult is TaskResult.Failure && networkFetchResult.codes == ErrorCodes.ACCESS_DENIED) {
                return@withContext UserDetailResult.AccessDenied
            }
            return@withContext UserDetailResult.OtherFailure
        }
    }

}