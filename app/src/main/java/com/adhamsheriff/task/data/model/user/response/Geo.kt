package com.adhamsheriff.task.data.model.user.response

data class Geo(
    val lat: String,
    val lng: String
)