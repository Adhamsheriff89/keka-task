package com.adhamsheriff.task.data.repository

import android.util.Log
import com.adhamsheriff.task.data.model.user.response.UserResponseItem
import com.adhamsheriff.task.network.NetworkService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject

class UserRepository @Inject constructor (private val networkService: NetworkService) {

    private val tag = "UsersResponse"


    suspend fun fetchUserList(): TaskResult<List<UserResponseItem>> {
        return withContext(Dispatchers.IO) {
            try {
                val response = networkService.getUsers()
                if (response.isSuccessful) {
                    val users = response.body()
                    if (users != null) {
                        return@withContext TaskResult.Success(users)
                    }
                }
                return@withContext TaskResult.Success(emptyList())
            } catch (e: Exception) {
                Log.d(tag, e.message.toString())
                return@withContext TaskResult.Failure(ErrorCodes.API_FAILURE)
            }
        }
    }



}