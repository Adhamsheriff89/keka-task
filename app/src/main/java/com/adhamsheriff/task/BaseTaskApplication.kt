package com.adhamsheriff.task

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BaseTaskApplication: Application() {
}