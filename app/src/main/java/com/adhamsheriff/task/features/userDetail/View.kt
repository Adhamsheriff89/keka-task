package com.adhamsheriff.task.features.userDetail


import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.adhamsheriff.task.R
import com.adhamsheriff.task.data.model.user.response.Address
import com.adhamsheriff.task.data.model.user.response.Company


@Composable
fun UserListView(
    viewModel: UserDetailViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsState()

    LaunchedEffect(key1 = Unit) {
        viewModel.initiateUserDetailFetching()
    }

    when (uiState) {
        is UserDetailUiState.Loading -> {
            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                CircularProgressIndicator(modifier = Modifier.size(56.dp))
            }
        }

        is UserDetailUiState.DisplayUserData -> {
            LazyColumn {
                items((uiState as UserDetailUiState.DisplayUserData).list) { user ->
                    UserCard(stats = user)
                }
            }
        }

        is UserDetailUiState.SomethingWrong -> {
            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Text(text = stringResource(id = R.string.something_wrong_text))
            }
        }

        else -> {}
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun UserCard(stats:UserUIData, modifier: Modifier = Modifier) {
    Card(
        modifier = modifier
            .fillMaxWidth()
            .padding(8.dp),
        onClick = {}
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            Text(text = stats.name, fontWeight = FontWeight.Bold)
            Spacer(modifier = Modifier.height(8.dp))
            Text(text = "Username: ${stats.userName}")
            Spacer(modifier = Modifier.height(4.dp))
            Text(text = "Email: ${stats.email}")
            Spacer(modifier = Modifier.height(4.dp))
            Text(text = "Phone: ${stats.phone}")
            Spacer(modifier = Modifier.height(4.dp))
            Text(text = "Website: ${stats.website}")
            Spacer(modifier = Modifier.height(8.dp))
            AddressSection(address = stats.address)
            Spacer(modifier = Modifier.height(8.dp))
            CompanySection(company = stats.company)
        }
    }
}

@Composable
fun AddressSection(address: Address) {
    Column {
        Text(stringResource(id = R.string.address_label), fontWeight = FontWeight.Bold)
        Spacer(modifier = Modifier.height(4.dp))
        Text("${stringResource(id = R.string.street_label)} ${address.street}")
        Spacer(modifier = Modifier.height(4.dp))
        Text("${stringResource(id = R.string.suite_label)} ${address.suite}")
        Spacer(modifier = Modifier.height(4.dp))
        Text("${stringResource(id = R.string.city_label)} ${address.city}")
        Spacer(modifier = Modifier.height(4.dp))
        Text("${stringResource(id = R.string.zipcode_label)} ${address.zipcode}")
    }
}

@Composable
fun CompanySection(company: Company) {
    Column {
        Text(stringResource(id = R.string.company_label), fontWeight = FontWeight.Bold)
        Spacer(modifier = Modifier.height(4.dp))
        Text("${stringResource(id = R.string.name_label)} ${company.name}")
        Spacer(modifier = Modifier.height(4.dp))
        Text("${stringResource(id = R.string.catchphrase_label)} ${company.catchPhrase}")
        Spacer(modifier = Modifier.height(4.dp))
        Text("${stringResource(id = R.string.bs_label)} ${company.bs}")
    }
}


@Composable
@Preview
fun UserListViewPreview() {
    UserListView()
}
