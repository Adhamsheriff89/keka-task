package com.adhamsheriff.task.features.userDetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.adhamsheriff.task.data.domain.UsersDataUseCase
import com.adhamsheriff.task.data.model.user.response.Address
import com.adhamsheriff.task.data.model.user.response.Company
import com.adhamsheriff.task.data.model.user.response.UserResponseItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserDetailViewModel @Inject constructor(
    private val usersDataUseCase: UsersDataUseCase
):ViewModel(){

    private val _uiState = MutableStateFlow<UserDetailUiState>(UserDetailUiState.Loading)
    val uiState = _uiState.asStateFlow()


    fun initiateUserDetailFetching() {
        viewModelScope.launch(Dispatchers.IO) {
            _uiState.value = UserDetailUiState.Loading

            when (val userData = usersDataUseCase.fetchUserListFromNetwork()) {
                is UsersDataUseCase.UserDetailResult.Success -> {
                    _uiState.value = UserDetailUiState.DisplayUserData(convertUserResponsesToUiData(userData.result))
                }
                else -> {
                    _uiState.value = UserDetailUiState.SomethingWrong
                    return@launch
                }
            }
        }
    }

    private fun convertUserResponsesToUiData(data: List<UserResponseItem>): List<UserUIData>{
        val list = ArrayList<UserUIData>()
        data.forEach {
            list.add(it.toUserUIData())
        }
        return list
    }

    private fun UserResponseItem.toUserUIData() = run {
        UserUIData(
            name = this.name,
            userName = this.username,
            email = this.email,
            city = this.address.city,
            phone = this.phone,
            website = this.website,
            address = this.address,
            company = this.company
        )
    }

}



sealed interface UserDetailUiState {
    data class DisplayUserData(
        val list: List<UserUIData>
    ) : UserDetailUiState
    data object Loading : UserDetailUiState
    data object NoDataFound : UserDetailUiState
    data object SomethingWrong : UserDetailUiState
}
data class UserUIData(
    val name:String,
    val userName :String,
    val email :String,
    val city :String,
    val phone:String,
    val website:String,
    val address:Address,
    val company:Company,
)
