package com.adhamsheriff.task.feature.userDetail


import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel


@Composable
fun UserListView(
    viewModel: UserDetailViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsState()


    when (uiState) {
        is UserDetailUiState.Loading -> {
            CircularProgressIndicator()
        }

        is UserDetailUiState.ShowUserData -> {
            LazyColumn(content = {
                items((uiState as UserDetailUiState.ShowUserData).list) {
                    Card {
                        Column {
                            Text(text = "Name: ${it.name}")
                            Text(text = "Email: ${it.email}")
                            Text(text = "City: ${it.city}")
                        }
                    }
                }
            })
        }

        is UserDetailUiState.SomethingWrong -> {
            // TODO error ui
        }

        else -> {}
    }
}
