package com.adhamsheriff.task.feature.userDetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.adhamsheriff.task.data.domain.UsersDataUseCase
import com.adhamsheriff.task.data.model.user.response.UserResponse
import com.adhamsheriff.task.data.model.user.response.UserResponseItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserDetailViewModel @Inject constructor(
    private val usersDataUseCase: UsersDataUseCase
):ViewModel(){

    private val _uiState = MutableStateFlow<UserDetailUiState>(UserDetailUiState.Loading)
    val uiState = _uiState.asStateFlow()

    init {
        startProcedure()
    }

    private fun startProcedure() {
        viewModelScope.launch(Dispatchers.IO) {
            _uiState.value = UserDetailUiState.Loading

           val userData = usersDataUseCase.getUsersDataFromNetwork()

            when (userData) {
                is UsersDataUseCase.UserDetailResult.Success -> {
                    _uiState.value = UserDetailUiState.ShowUserData(prepareUiData(userData.result))
                }
                else -> {
                    _uiState.value = UserDetailUiState.SomethingWrong
                    return@launch
                }
            }
        }
    }

    private fun prepareUiData(data: List<UserResponse>): List<UserUIData>{
        val list = ArrayList<UserUIData>()
        data.forEach {
            it.map { list.add(it.convertToUserUIData())  }
        }
        return list
    }

    private fun UserResponseItem.convertToUserUIData() = run {
        UserUIData(
            name = this.name,
            userName = this.username,
            email = this.email,
            city = this.address.city
        )
    }

}



sealed interface UserDetailUiState {
    data class ShowUserData(
        val list: List<UserUIData>
    ) : UserDetailUiState
    object Loading : UserDetailUiState
    object NoDataFound : UserDetailUiState
    object SomethingWrong : UserDetailUiState
}
data class UserUIData(
    val name:String,
    val userName :String,
    val email :String,
    val city :String,
)
