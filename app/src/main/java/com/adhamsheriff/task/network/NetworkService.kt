package com.adhamsheriff.task.network

import com.adhamsheriff.task.data.model.user.response.UserResponseItem
import retrofit2.Response
import retrofit2.http.GET


const val BASE_URL = "https://jsonplaceholder.typicode.com/"

interface NetworkService {
    @GET("users")
    suspend fun getUsers(): Response<List<UserResponseItem>>
}