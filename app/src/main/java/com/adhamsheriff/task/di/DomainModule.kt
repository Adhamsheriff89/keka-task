package com.adhamsheriff.task.di

import com.adhamsheriff.task.data.domain.UsersDataUseCase
import com.adhamsheriff.task.data.repository.UserRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DomainModule {

    @Provides
    @Singleton
    fun providesUsersDataUseCase(userRepository: UserRepository):UsersDataUseCase{
        return UsersDataUseCase(userRepository)
    }
}