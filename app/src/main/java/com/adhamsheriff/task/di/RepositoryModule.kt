package com.adhamsheriff.task.di

import com.adhamsheriff.task.data.repository.UserRepository
import com.adhamsheriff.task.network.NetworkService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
object RepositoryModule {

    @Provides
    fun provideUserRepository(networkService: NetworkService): UserRepository {
        return UserRepository(networkService)
    }

}